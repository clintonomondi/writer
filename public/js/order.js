
document.getElementById("totaltxt").textContent='0.0';
// document.getElementById("totaltxt2").textContent='0.0';
localStorage.clear();

localStorage.setItem('service','0');
localStorage.setItem('paper','0');
localStorage.setItem('agency','0');
localStorage.setItem('academics','0');
localStorage.setItem('pages','0');
localStorage.setItem('writerlevel','0');
localStorage.setItem('powerpointcheckvalue','0');
localStorage.setItem('writer','0');
localStorage.setItem('additonal','0');
localStorage.setItem('additonal2','0');
localStorage.setItem('customer_service','0');
localStorage.setItem('subject','0');

localStorage.setItem('math','1');
resetForm();
calculateDefaultTotal(1);

function  calculateDefaultTotal(math) {
    var pages= 1  * +math;
    localStorage.setItem('pages',pages);
    addTotal();
}
function addpowerpointValue(val) {
    localStorage.setItem('powerpointcheckvalue',val);
    addTotal();
}

function TypeOfservice(id,data) {
    var TypeOfservice =data.find(TypeOfservice => TypeOfservice.id === +id);
    localStorage.setItem('service',TypeOfservice.amount);
    addTotal();
    loadAcademics(TypeOfservice.name)
}

function TypeofPaper(id,data) {
    var TypeofPaper =data.find(TypeofPaper => TypeofPaper.id === +id);
    localStorage.setItem('paper',TypeofPaper.amount);
    addTotal();
}
function subject(id,data) {
    var subject =data.find(subject => subject.id === +id);
    localStorage.setItem('subject',subject.amount);
    addTotal();
}
function topic(value) {
    document.getElementById("topic").textContent=value;
}

function agency(id,data) {
    var agency =data.find(agency => agency.id === +id);
    localStorage.setItem('agency',agency.amount);

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    document.getElementById('datefd').value=dateTime;
    addTotal();
}

function academics(id,data) {
    var item =data.find(item => item.id === +id);
    localStorage.setItem('academics',item.amount);
    addTotal();
}

function writerlevel(val) {

    localStorage.setItem('writerlevel',val);
    addTotal();
}

function numberofpages(value) {
    var math=localStorage.getItem('math');
    var pages= +value  * +math;
    localStorage.setItem('pages',pages);
    addTotal();
}
function additional(amount,id) {
    if(document.getElementById('additional'+id).checked) {
        var additonal=localStorage.getItem('additonal');
        var newadditional=+additonal+ +amount;
        localStorage.setItem('additonal',newadditional);
        addTotal();
    }else{
        var additonal2=localStorage.getItem('additonal');
        var newadditional2=+additonal2 - +amount;
        localStorage.setItem('additonal',newadditional2);
        addTotal();
    }
}

function additional_other(amount,id) {
    if(document.getElementById('additional'+id).checked) {
        var additonal2=localStorage.getItem('additonal2');
        var newadditional2=+additonal2+ +amount;
        localStorage.setItem('additonal2',newadditional2);
        addTotal();
    }else{
        var additonal3=localStorage.getItem('additonal2');
        var newadditional3=+additonal3 - +amount;
        localStorage.setItem('additonal2',newadditional3);
        addTotal();
    }
}




function addTotal() {
    var service=localStorage.getItem('service');
    var paper=localStorage.getItem('paper');
    var agency=localStorage.getItem('agency');
    var academics=localStorage.getItem('academics');
    var pages=localStorage.getItem('pages');
    var writerlevel=localStorage.getItem('writerlevel');
    var powerpointcheckvalue=localStorage.getItem('powerpointcheckvalue');
    var writer=localStorage.getItem('writer');
    var additonal=localStorage.getItem('additonal');
    var additonal2=localStorage.getItem('additonal2');
    var customer_service=localStorage.getItem('customer_service');
    var subject=localStorage.getItem('subject');

    var special_additional=additonal2 * +pages;
    var total=((+service+ +paper+ +academics+ +subject)* +pages) + (+writerlevel+ +powerpointcheckvalue+ +writer+ +additonal+ +special_additional+ +customer_service);

    var urgency_ded=(agency/100)* +total;
    var real_total=+urgency_ded+ +total;




    // document.getElementById('add_id').innerText='$ '+special_additional.toFixed(2);
    //
    // document.getElementById('service_value').value=service;
    // document.getElementById('paper_value').value=paper;
    // document.getElementById('agency_value').value=urgency_ded;
    // document.getElementById('academics_value').value=academics;
    // document.getElementById('pages_value').value=pages;
    // document.getElementById('writerlevel_value').value=writerlevel;
    // document.getElementById('powerpointcheckvalue_value').value=powerpointcheckvalue;
    // document.getElementById('writer_value').value=writer;
    // document.getElementById('additonal_value').value=additonal;
    // document.getElementById('additonal2_value').value=special_additional;
    // document.getElementById('customer_service_value').value=customer_service;
    // document.getElementById('subject_value').value=subject;
    //
    // var disc=document.getElementById('discount1').value;
    // var real_disc=(+disc * +real_total)/100;
    // document.getElementById('disc').innerText='$ '+real_disc.toFixed(2);
    // var amount_to_pay=+real_total - +real_disc;
    // document.getElementById("totalfd").value=amount_to_pay.toFixed(2);
    // document.getElementById("totaltxt").textContent=amount_to_pay.toFixed(2);
    // document.getElementById("amount").textContent=real_total.toFixed(2);
}

function resetForm() {
    document.getElementById("orderForm").reset();
}

function loopPagesField(val,math) {
    localStorage.setItem('math',math);
    $("#pages").empty();
    var select = document.getElementById("pages");
    var i;
    for (i = 1; i <= 200; i++) {
        var words=val * i;
        $(select).append('<option value=' + i + '>' + i  + '  Page(s)/ '+words+'</option>');
    }
    $(select).val([1]);
    var pages= 1 * +math;
    localStorage.setItem('pages',pages);
    addTotal();
}
function loopPagesFieldQuestion(val,math) {
    localStorage.setItem('math',math);
    $("#pages").empty();
    var select = document.getElementById("pages");
    var i;
    for (i = 1; i <= 200; i++) {
        var words=val * i;
        $(select).append('<option value=' + i + '>' + i  + '   Questions</option>');
    }
    $(select).val([1]);
    var pages= 1 * +math;
    localStorage.setItem('pages',pages);
    addTotal();
}

function loopPowerpointPages() {
    $("#powerpointdrop").empty();
    var select = document.getElementById("powerpointdrop");
    var i;
    for (i = 1; i <= 500; i++) {
        $(select).append('<option value=' + i + '>' + i  + '  Slide(s)</option>');
    }
    $(select).val([1]);
}

function calculateePowerPoint(val) {
    var hidden=document.getElementById('hidden_powerpoint').value;
    var t=val * +hidden;
    document.getElementById("exampleRadios1").innerHTML ='$'+t;
    // document.getElementById("switchColorOpt").value =t;
    addpowerpointValue(t);
}

function calculateWriter(val) {
    localStorage.setItem('writer',val);
    addTotal();
}

function calculateCustomerService(val) {
    localStorage.setItem('customer_service',val);
    addTotal();
}



