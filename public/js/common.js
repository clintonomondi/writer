$('#submitbtn').attr("disabled",true);  //Initially disabled button when document loaded.
$('#check').click(function(){
    $('#submitbtn').attr("disabled",!$(this).is(":checked"));
})

$('.submitbtn').attr("disabled",true);  //Initially disabled button when document loaded.
$('.check').click(function(){
    $('.submitbtn').attr("disabled",!$(this).is(":checked"));
})

function loadbutton() {
    $('#submitbtn').attr('disabled', true);
    $('#submitbtn').text('Please wait..');
}

function stopbutton() {
    $('#submitbtn').attr('disabled', false);
    $('#submitbtn').text('Submit');
}


function loadbutton2() {
    $('.submitbtn').attr('disabled', true);
    $('.submitbtn').text('Please wait..');
}

function stopbutton2() {
    $('.submitbtn').attr('disabled', false);
    $('.submitbtn').text('Submit');
}

function loadbutton3() {
    $('.submitbtn3').attr('disabled', true);
    $('.submitbtn3').text('Please wait..');
}

function stopbutton3() {
    $('.submitbtn3').attr('disabled', false);
    $('.submitbtn3').text('Submit');
}

function loadchatBtn() {
    $('#chat_btn').attr('disabled', true);
    $('#chat_btn').text('Sending...');
}

function stopchatBtn() {
    $('#chat_btn').attr('disabled', false);
    $('#chat_btn').text('Send');
}
function  showtimeTime(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    // document.getElementById('datefd').value=dateTime;
    return dateTime;
}


function dateToNiceString(myDate){
    var month=new Array();
    month[0]="Jan";
    month[1]="Feb";
    month[2]="Mar";
    month[3]="Apr";
    month[4]="May";
    month[5]="Jun";
    month[6]="Jul";
    month[7]="Aug";
    month[8]="Sep";
    month[9]="Oct";
    month[10]="Nov";
    month[11]="Dec";
    var hours = myDate.getHours();
    var minutes = myDate.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ampm;
    return month[myDate.getMonth()]+" "+myDate.getDate()+" "+myDate.getFullYear()+" "+strTime;
}
function datatable() {
    $(document).ready( function () {
        $('.table').DataTable();
    } );
}
