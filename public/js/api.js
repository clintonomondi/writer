$('#slip-div').hide();
function checkpowerpountRadio(val) {
    if(document.getElementById('switchColorOpt').checked) {
        $('#slip-div').show();

        loopPowerpointPages();
        addpowerpointValue(val);
    } else{
        document.getElementById("exampleRadios1").innerHTML ='$ '+val;
        // document.getElementById("switchColorOpt").value =t;
        addpowerpointValue('0');
        $('#slip-div').hide();
    }
}
// switchContact();
function switchContact() {
    if(document.getElementById('registered').checked) {
        $('#extra').hide();
        $('#extra2').hide();
    }else if(document.getElementById('new').checked){
        $('#extra').show();
        $('#extra2').show();
    }
}




$( document ).ready(function() {
    getDefaultOrderService();

});

function  loadAcademics(name) {
    // $.LoadingOverlay("show");
    var data;
    data = new FormData();
    data.append('name',name);
    $.ajax({
        url:'/api/auth/client/loadAcademics',
        method:"POST",
        data: data,
        contentType:false,
        processData:false,
        success:function(data) {
            $("#academic_id").empty();
            var select = document.getElementById("academic_id");
            $(select).append('<option selected="true" disabled="disabled" >Select academic level</option>');
            $(data.academic).each(function (i,value) {
                $(select).append('<option value=' + value.id + '>' + value.name +'</option>');
            });
            if(data.dis==='no'){
                $(select).val([1]);
                academics(1,data.academic);
            }else{
                $(select).val([data.academic_id.id]);
                academics(data.academic_id.id,data.academic);

                $("#typeofpaper_id").empty();
                var select3 = document.getElementById("typeofpaper_id");
                $(select3).append('<option selected="true" disabled="disabled" >Select paper</option>');
                $(data.paper).each(function (i,value) {
                    $(select3).append('<option value=' + value.id + '>' + value.name +'</option>');
                });
                $(select3).val([data.paper_id.id]);
                TypeofPaper(data.paper_id.id,data.paper);
            }

            $("#agency_id").empty();
            var select2 = document.getElementById("agency_id");
            $(select2).append('<option selected="true" disabled="disabled" >Select urgency</option>');
            $(data.agency).each(function (i,value) {
                $(select2).append('<option value=' + value.id + '>' + value.value +'   '+value.prefix+'</option>');
            });
            $(select2).val([data.agency_id.id]);
            agency(data.agency_id.id,data.agency);

            if(data.question==='yes'){
                loopPagesFieldQuestion(275,1);
            }else{
                loopPagesField(275,1);
            }
            // $.LoadingOverlay("hide");

        },
        error:function(data) {
            // $.LoadingOverlay("hide");
            // swal({
            //     text: 'System technical error, reload your page',
            //     icon: "error",
            // });
        }

    });

}
function  getDefaultOrderService() {
    $.ajax({
        url:'/api/auth/client/order',
        method:"GET",
        contentType:false,
        processData:false,
        success:function(data) {
            $("#typeofservice_id").empty();
            var select = document.getElementById("typeofservice_id");
            $(select).append('<option selected="true" disabled="disabled" >Select service</option>');
            $(data.services).each(function (i,value) {
                $(select).append('<option value=' + value.id + '>' + value.name +'</option>');
            });
            $(select).val([1]);
            TypeOfservice(1,data.services);


            $("#typeofpaper_id").empty();
            var select2 = document.getElementById("typeofpaper_id");
            $(select2).append('<option selected="true" disabled="disabled" >Select paper</option>');
            $(data.paper).each(function (i,value) {
                $(select2).append('<option value=' + value.id + '>' + value.name +'</option>');
            });

            $(select2).val([1]);
            TypeofPaper(1,data.paper);
        },
        error:function() {
            // swal({
            //     text: 'System technical error, contact support',
            //     icon: "error",
            // });
        }

    });
}
