<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Writer_Eslips extends Model
{
    use HasFactory;

    protected  $fillable=['eslip_no','user_id','amount','records','created_by'];

    public  function user(){
        return $this->belongsTo(User::class);
    }
}
