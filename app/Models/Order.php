<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected  $fillable=['typeofservice_id','typeofpaper_id','subject_id','instruction','title','agency_id','academic_id','pages',
        'writerlevel_id','prefered_id','discount_code','user_id','status','amount','code'   ,'powerpointchecked',
        'powerpointslides','style','pagespacing','sources','writer',
        'customer_service','deadline','submited_at','logins','writer_id','taken'];

}
