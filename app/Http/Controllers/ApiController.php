<?php

namespace App\Http\Controllers;

use App\Models\Academic;
use App\Models\Agency;
use App\Models\Order;
use App\Models\PagesPowerpoint;
use App\Models\Typeofpaper;
use App\Models\Typeofservice;
use App\Models\Wallet;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class ApiController extends Controller
{






    public  function getOrderData(){
        $id=Auth::user()->id;
        $pending=Order::where('taken','No')->where('status','=','inprogress')->count();
        $inprogress=Order::where('status','inprogress')->where('writer_id',Auth::user()->id)->count();
        $revised=Order::where('status','revised')->where('writer_id',Auth::user()->id)->count();
        $completed=Order::where('status','completed')->where('writer_id',Auth::user()->id)->count();
        $onhold=Order::where('status','onhold')->where('writer_id',Auth::user()->id)->count();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        $amount = DB::select( DB::raw("SELECT SUM(amount)AS amount FROM orders WHERE STATUS='completed'  AND writer_id='$id' AND id NOT IN (SELECT order_id FROM writer__billings WHERE user_id='$id')") );
        $wallet=$amount[0]->amount *$percent/100;
        return ['status'=>true,'pending'=>$pending,'inprogress'=>$inprogress,
            'revised'=>$revised,'completed'=>$completed,'onhold'=>$onhold,'wallet'=>$wallet];
    }

}
