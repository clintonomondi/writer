<?php

namespace App\Http\Controllers;

use App\Models\Otp;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);


        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }


    public function updateInfo(Request $request)
    {
       $user=User::find(Auth::user()->id);
       $user->update($request->all());
        return ['status'=>true,'message'=>'Information updated successfully'];
    }

    public  function updatePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_set']='1';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());

        $Notif_Api=env('Notif_Api');
        $data=['message'=>'Your password was successfully changed ', 'email'=>$user->email, 'subject'=>'PASSWORD CHANGED'];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api,$data);
        return ['status'=>true,'message'=>'Password successfully set, please use new password to login again'];
    }
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

public  function getOtp(Request $request){
    $Notif_Api=env('Notif_Api');
        $user=User::where('email',$request->user_id)->first();
        $count=User::where('email',$request->user_id)->where('role','writer')->count();
        if($count<=0){
            return ['status'=>false,'message'=>'This email does not exist as a writer'];
        }

    $randomid = mt_rand(100000,999999);
    $request['code']=$randomid;
    $data=OTP::create($request->all());

    $data=['message'=>'Your password reset code is  '.$randomid, 'email'=>$request->user_id, 'subject'=>'FORGET PASSWORD'];
    $data2=['message'=>'Your password reset code is  '.$randomid.'.  @DevMyEssay','phone'=>$user->phone];
    $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'email',$data);
    $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);



    return ['status'=>true,'message'=>'Code sent to your phone and email','user'=>$user];


}

public  function verify(Request $request){
    $datas = DB::select( DB::raw("SELECT * FROM `otps` WHERE user_id='$request->email' AND code='$request->code'") );
    if($datas==null){
        return ['status'=>false,'message'=>'Invalid code'];
    }
    return ['status'=>true,'message'=>'Success successfully'];
}

public  function resetpassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }
    $request['password']=bcrypt($request->password);
    $email=User::where('email',$request->email)->update(['password'=>$request->password]);

    $Notif_Api=env('Notif_Api');
    $data=['message'=>'Your password was successfully changed ', 'email'=>$request->user_id, 'subject'=>'PASSWORD CHANGED'];
    $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api,$data);


    return ['status'=>true,'message'=>'Password successfully set, please new password to login'];
}
}
