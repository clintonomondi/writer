<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use App\Models\Writer_Billing;
use App\Models\Writer_Eslips;
use Illuminate\Http\Request;
use App\Events\MessageSent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function fetchMessages($id)
    {
        $count=Message::where('user_id','!=',Auth::user()->id)->where('status','1')->where('order_id',$id)->count();
        $data=Message::where('order_id',$id)->get();
        $my_id=Auth::user()->id;
        return ['data'=>$data,'count'=>$count,'my_id'=>$my_id];
    }


    public function sendMessage(Request $request,$id)
    {
       $request['order_id']=$id;
       $request['user_id']=Auth::user()->id;
       $data=Message::create($request->all());
        $update=Message::where('user_id','!=',Auth::user()->id)->where('order_id',$id)->update(['status'=>'0']);
        return ['status' => 'Message Sent!'];
    }
    public  function updateNotif($id){
        $update=Message::where('user_id','!=',Auth::user()->id)->where('order_id',$id)->update(['status'=>'0']);
        return ['status' => 'Sent!'];
    }

    public  function notifications(){
        $id=Auth::user()->id;
        $notif=DB::select( DB::raw("SELECT *,
(SELECT code FROM orders B WHERE B.id=A.order_id)code
 FROM `messages` A WHERE user_id!='$id' AND STATUS='1' AND order_id IN (SELECT order_id FROM orders WHERE writer_id='$id')") );
        return ['notif'=>$notif];
    }

    public  function eslips(){
        $id=Auth::user()->id;
        $eslips=Writer_Eslips::where('user_id',$id)->get();
        return ['status'=>true,'eslips'=>$eslips];
    }

    public  function eslip_bills($id){
        $eslip=Writer_Eslips::find($id);
        $bills=DB::select( DB::raw("SELECT *,
(SELECT CODE FROM orders B WHERE B.id=A.order_id)code
 FROM `writer__billings` A WHERE eslip_no='$eslip->eslip_no' ") );
        return ['status'=>true,'eslips'=>$bills];
    }
}
