<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('getOtp', 'App\Http\Controllers\AuthController@getOtp');
    Route::post('verify', 'App\Http\Controllers\AuthController@verify');
    Route::post('resetpassword', 'App\Http\Controllers\AuthController@resetpassword');


    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');
        Route::post('updateInfo', 'App\Http\Controllers\AuthController@updateInfo');
        Route::post('updatePassword', 'App\Http\Controllers\AuthController@updatePassword');

        Route::get('/client/getOrderData', 'App\Http\Controllers\ApiController@getOrderData')->name('student.getOrderData');
        Route::get('/client/orders/view/{id}', 'App\Http\Controllers\OrdersController@vieworder')->name('student.vieworder');
        Route::get('/client/orders', 'App\Http\Controllers\OrdersController@orders')->name('student.orders');
        Route::get('/client/inprogress', 'App\Http\Controllers\OrdersController@inprogress')->name('student.inprogress');
        Route::get('/client/onhold', 'App\Http\Controllers\OrdersController@onhold')->name('student.onhold');
        Route::get('/client/revised', 'App\Http\Controllers\OrdersController@revised')->name('student.revised');
        Route::get('/client/completed', 'App\Http\Controllers\OrdersController@completed')->name('student.completed');
        Route::get('/order/bid/{id}', 'App\Http\Controllers\OrdersController@bid');
        Route::post('/order/upload/{id}', 'App\Http\Controllers\OrdersController@upload');

        Route::get('messages/{id}', 'App\Http\Controllers\ChatsController@fetchMessages');
        Route::post('messages/{id}', 'App\Http\Controllers\ChatsController@sendMessage');
        Route::get('notifications', 'App\Http\Controllers\ChatsController@notifications');
        Route::get('updateNotif/{id}', 'App\Http\Controllers\ChatsController@updateNotif');
        Route::get('eslips', 'App\Http\Controllers\ChatsController@eslips');
        Route::get('eslip_bills/{id}', 'App\Http\Controllers\ChatsController@eslip_bills');

    });
});
