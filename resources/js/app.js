import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import Notifications from 'vue-notification'


Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(Notifications)

import App from './view/App'
import Index from './pages/index'
import About from './pages/about'
import Login from './pages/login'

import Home from './view/home'
import Pending from './orders/pending'
import Inprogress from './orders/inprogress'
import Onhold from './orders/onhold'
import Revised from './orders/revised'
import Eslips from './orders/eslips'
import EslipBills from './orders/bills'
import Completed from './orders/completed'
import Detail from './orders/view'
import Refunds from './orders/refunds'
import Chat from './orders/chat'
import Forget from './pages/forget_password'
import Reset_Pass from './pages/reset_password'
import Profile from './pages/profile'
import Confirm_Pass from './pages/confirm'
import Set_password from './pages/set_password'




const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },


        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/pending',
            name: 'pending',
            component: Pending
        },
        {
            path: '/inprogress',
            name: 'inprogress',
            component: Inprogress
        },
        {
            path: '/onhold',
            name: 'onhold',
            component: Onhold
        },
        {
            path: '/revised',
            name: 'revised',
            component: Revised
        },
        {
            path: '/completed',
            name: 'completed',
            component: Completed
        },
        {
            path: '/detail/:id',
            name: 'detail',
            component: Detail
        },
        {
            path: '/refunds',
            name: 'refunds',
            component: Refunds
        },


        {
            path: '/chat/:id',
            name: 'chat',
            component: Chat
        },

        {
            path: '/forget',
            name: 'forget',
            component: Forget
        },

        {
            path: '/reset_pass',
            name: 'reset_pass',
            component: Reset_Pass
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/confirm_pass',
            name: 'confirm_pass',
            component: Confirm_Pass
        },
        {
            path: '/set_password',
            name: 'set_password',
            component: Set_password
        },
        {
            path: '/eslips',
            name: 'eslips',
            component: Eslips
        },
        {
            path: '/eslipbills/:id',
            name: 'eslipbills',
            component: EslipBills
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

