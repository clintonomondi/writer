<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from html.lionode.com/batota/b001/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Nov 2020 08:42:26 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DevMyEssay</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/images/logo.png">
    <!-- CSS
    ========================= -->
    <!-- Bootstrap CSS -->
    <script src="/assets/js/vendor/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="/assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="/assets/css/ionicons.min.css">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="/assets/css/plugins.css">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- Modernizer JS -->
    <script src="/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css
">

    <link rel="stylesheet" type="text/css" href="{{asset('css/filechooser.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/home.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/chat.css')}}" />
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>


</head>
<body>
<div id="app">
    <app></app>
</div>



<script src="{{ mix('js/app.js') }}"></script>


<!-- jQuery JS -->

<!-- Popper JS -->
<script src="/assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- Plugins JS -->
<script src="/assets/js/plugins.js"></script>
<!-- Ajax Mail -->
<script src="/assets/js/ajax-mail.js"></script>
<!-- Main JS -->
<script src="/assets/js/main.js"></script>

<script src="{{asset('js/common.js')}}"></script>

</body>

<!-- Mirrored from html.lionode.com/batota/b001/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Nov 2020 08:42:26 GMT -->
</html>
